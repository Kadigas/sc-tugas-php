<?php 
    require('Ape.php');
    require('Frog.php');
    
    $sheep = new Animal("shaun");

    $sheep->get_name(); // "shaun"
    $sheep->get_legs(); // 4
    $sheep->get_cold_blooded(); // "no"
    echo "<br>";

    $kodok = new Frog("buduk");
    $kodok->jump(); // "hop hop"
    echo "<br>";

    $sungokong = new Ape("kera sakti");
    $sungokong->yell(); // "Auooo"
?>