<?php
    require_once('animal.php');
    class Ape extends Animal
    {
    protected $legs = 2;
    public function __construct($name){ 
        parent::__construct($name);
        $this->get_name();
        $this->get_legs();
        $this->get_cold_blooded();
    }
    public function yell(){
        echo "Yell: Auooo<br>";
    }
    }
?>