<?php
    require_once('animal.php');
    class Frog extends Animal
    {
    public function __construct($name){ 
        parent::__construct($name);
        $this->get_name();
        $this->get_legs();
        $this->get_cold_blooded();
    }
    public function jump(){
        echo "Jump: Hop Hop<br>";
    }
    }
?>